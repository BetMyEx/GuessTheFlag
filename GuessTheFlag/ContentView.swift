//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Admin on 02.01.2022.
//

import SwiftUI

struct FlagImage: View {
    var flag: String
    
    var body: some View {
        Image(flag)
            .renderingMode(.original)
            .clipShape(Capsule())
            .shadow(radius: 5)
    }
}

struct TitleStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle.bold())
            .foregroundColor(.blue)
    }
}
extension View {
    func titleStyle() -> some View {
        modifier(TitleStyle())
    }
}
struct ContentView: View {
    @State private var currentScore = 0
    @State private var currentStep = 0
    @State private var showingScore = false
    @State private var showingFinalScore = false
    @State private var scoreTitle = ""
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var selectedFlag = 0
    @State private var animationAmount = 0.0
    @State private var opacityAmount = 1.0
    @State private var scaleAmount = 1.0
    var body: some View {
        ZStack{
            RadialGradient(stops: [
                .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3),
                .init(color: Color(red: 0.45, green: 0.15, blue: 0.26), location: 0.3)], center: .top, startRadius: 200, endRadius: 700)
                .ignoresSafeArea()
            VStack{
                Spacer()
                Text("Guess the flag")
                    .font(.largeTitle.bold())
                    .foregroundColor(.white)
                VStack(spacing: 15){
                    VStack{
                        Text("Tap the flag of")
                            .foregroundStyle(.secondary)
                            .font(.subheadline.weight(.heavy))
                        Text(countries[correctAnswer])
                            .font(.largeTitle.weight(.semibold))
                    }
                    ForEach(0..<3) {number in
                        Button {
                            flagTapped(number)
                        } label: {
                            FlagImage(flag: countries[number])
                        }
                        .rotation3DEffect(.degrees(number == selectedFlag ? animationAmount: 0), axis: (x: 0, y: 1, z: 0))
                        .opacity(Double(number == selectedFlag ? 1.0 : opacityAmount))
                        .scaleEffect(Double(number == selectedFlag ? 1.0 : scaleAmount))
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .background(.thinMaterial)
                .clipShape(RoundedRectangle(cornerRadius: 20))
                Spacer()
                Spacer()
                Text("Your score \(currentScore)")
                    .titleStyle()
                Spacer()
            }
            .padding()
        }
        .alert(scoreTitle, isPresented: $showingScore){
            Button("Continue", action: askQuestion)
        } message: {
            Text("Your current score is \(currentScore)")
        }
        .alert(scoreTitle, isPresented: $showingFinalScore){
            Button("Start new game", action: reset)
        } message: {
            Text("Your final score is \(currentScore)")
        }
    }
    func flagTapped(_ number: Int) {
        if number == correctAnswer {
            selectedFlag = number
            scoreTitle = "Correct"
            currentScore += 1
            currentStep += 1
            showingScore = true
            
            
        } else {
            selectedFlag = number
            scoreTitle = "Wrong, this is flag of \(countries[number])"
            currentStep += 1
            showingScore = true
            
        }
        withAnimation{
            self.animationAmount += 360
            self.opacityAmount -= 0.75
            self.scaleAmount -= 0.5
        }
    }
    func  askQuestion() {
        if currentStep == 8 {
            scoreTitle = "Game over"
            showingFinalScore = true
        } else {
            countries.shuffle()
            correctAnswer = Int.random(in: 0...2)
            opacityAmount = 1.0
            scaleAmount = 1.0
        }
    }
    func reset() {
        currentStep = 0
        currentScore = 0
        askQuestion()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
